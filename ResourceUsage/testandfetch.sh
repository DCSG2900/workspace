#!/bin/bash

CURRENT_MESH='linkerd'
METHOD='GET'

function prep_vegeta() {
  kubectl apply -f "$VEGETA_DIR/results-storage-volume.yml"
  kubectl apply -f "$VEGETA_DIR/vegeta-results-claim.yml"
  kubectl apply -n loadgen -f "$VEGETA_DIR/vegeta-config.yaml"
}

#Launch vegeta(METHOD, TARGET, RATE, DURATION, SAVE_OUTPUT, NAME)
function launch_vegeta() {
  #Get positional arguments
  local arg_method=$1
  local arg_target=$2
  local arg_rate=$3
  local arg_duration=$4
  local arg_save_output=$5
  local arg_name=$6

  #Prepare vegeta output argument
  local vegeta_output='stdout'
  if [[ $arg_save_output != 'false' ]]; then #output was specified so we need to save it
    local vegeta_output="/results/$CURRENT_MESH-$arg_name"

    #Check if local output does exist.
    if [[ -f $arg_save_output ]]; then
      echo "ERROR SPECIFIED OUTPUT FILE ALREADY EXISTS! STOPPING"
      exit 1
    fi
  fi

  #Configure vegeta
  local config='{"data":{"DURATION": "'$arg_duration'", "RATE":"'$arg_rate'", "METHOD":"'$arg_method'", "TARGET":"'$arg_target'", "OUTPUT":"'$vegeta_output'", "NAME":"'"$arg_name"'"}}'
  kubectl patch cm vegeta-config -n loadgen --type merge -p "$config"
  if [[ $? -ne 0 ]]; then
    echo "Failed to configure vegeta, tried to apply the following config:"
    echo "$config"
    exit 2
  fi

  #Launch vegeta
  kubectl -n loadgen apply -f "$VEGETA_DIR/vegeta-ha.yml"
  sleep 5 #Give vegeta some time to start
}

prep_vegeta
./annotator.sh "key" "localhost" "vegeta load 1000RPS @ 20m" "$CURRENT_MESH" "20m"
launch_vegeta $METHOD 'http://web-svc.emojivoto/api/leaderboard' 200 '20m' false "$CURRENT_MESH-HA"
echo "Wait 20 minutes and check grafana"
