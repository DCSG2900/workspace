#!/bin/bash
function create_annotation() {
  duration=$(sed -e 's/s$/ sec/g' -e 's/h$/ hour/g' -e 's/m$/ min/g' <<< "$3")
  cat <<EOT
  {
    "text": "$1",
    "tags": ["vegeta","$2"],
    "time": $(date +%s%3N),
    "timeEnd": $(date --date "now + $duration" +%s%3N),
    "isRegion": false
  }
EOT
}

key="$0"
endpoint="$1"
name="$2"
tag="$3"
duration="$4"

echo $(create_annotation $name $tag $duration)
curl -i \
  -H "Accept: Application/json" \
  -H "Authorization: Bearer $key" \
  -H "Content-Type: Application/json" \
  -X POST --data "$(create_annotation $name $tag $duration)" "http://$endpoint/api/annotations"
