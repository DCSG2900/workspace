#!/bin/bash

CURRENT_MESH='linkerd'
METHOD='GET'

function prep_vegeta() {
  kubectl apply -f "$VEGETA_DIR/results-storage-volume.yml"
  kubectl apply -f "$VEGETA_DIR/vegeta-results-claim.yml"
  kubectl apply -n loadgen -f "$VEGETA_DIR/vegeta-config.yaml"
}

#Launch vegeta(METHOD, TARGET, RATE, DURATION, SAVE_OUTPUT, NAME)
function launch_vegeta() {
  #Get positional arguments
  local arg_method=$1
  local arg_target=$2
  local arg_rate=$3
  local arg_duration=$4
  local arg_save_output=$5
  local arg_name=$6

  #Prepare vegeta output argument
  local vegeta_output='stdout'
  if [[ $arg_save_output != 'false' ]]; then #output was specified so we need to save it
    local vegeta_output="/results/$CURRENT_MESH-$arg_name"

    #Check if local output does exist.
    if [[ -f $arg_save_output ]]; then
      echo "ERROR SPECIFIED OUTPUT FILE ALREADY EXISTS! STOPPING"
      exit 1
    fi
  fi

  #Configure vegeta
  local config='{"data":{"DURATION": "'$arg_duration'", "RATE":"'$arg_rate'", "METHOD":"'$arg_method'", "TARGET":"'$arg_target'", "OUTPUT":"'$vegeta_output'", "NAME":"'"$arg_name"'"}}'
  kubectl patch cm vegeta-config -n loadgen --type merge -p "$config"
  if [[ $? -ne 0 ]]; then
    echo "Failed to configure vegeta, tried to apply the following config:"
    echo "$config"
    exit 2
  fi

  #Launch vegeta
  kubectl -n loadgen apply -f "$VEGETA_DIR/vegeta-save.yml"
  sleep 5 #Give vegeta some time to start

  #Wait for vegeta to complete
  job_pod=$(kubectl -n loadgen get pods --selector=job-name=vegeta --output=jsonpath='{.items[*].metadata.name}')
  echo "Job pod: $job_pod"

  #We cant use kubectl wait due to a linkerd-proxy never shutting down in batch jobs.. So we have to do this
  echo -n "Waiting for job to finnish"
  while [[ $(kubectl get pods "$job_pod" -n loadgen -o jsonpath='{.status.containerStatuses[?(@.name=="vegeta")].state}' | jq '.terminated | has("reason")') == false ]]; do
    echo -n '.'
    sleep 5
  done
  echo ""

  if [[ $arg_save_output != 'false' ]]; then
    #local volume_name=$(kubectl -n loadgen get pvc vegeta-results-claim -o jsonpath='{.spec.volumeName}')
    #local remote_path=$(kubectl get pv $volume_name -o jsonpath='{.spec.hostPath.path}')
    ##local remote_addr=$()
    echo "Spawning data access pod"
    kubectl apply -f "$VEGETA_DIR/results-access-pod.yml"
    sleep 10
    echo "Copying result to local storage: $arg_save_output"
    kubectl -n loadgen cp "results-dataaccess:/mnt/$CURRENT_MESH-$arg_name" "$arg_save_output"
    echo "Cleaning up data access pod"
    kubectl delete -f "$VEGETA_DIR/results-access-pod.yml"
  fi

  #Cleanup job
  echo "Cleaning up job"
  kubectl -n loadgen delete job/vegeta
}

##prep_vegeta
launch_vegeta $METHOD 'http://web-svc.emojivoto' 1000 '1m' "$WORKSPACE_DIR/results/Latency/$CURRENT_MESH-r1" 'r1'
