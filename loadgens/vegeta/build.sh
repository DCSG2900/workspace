#!/bin/bash

VERSION=latest

#Compile vegeta
make -C src

#Make sure compilation was successfull and that the output was as expected
if [[ $? -ne 0 ]]; then
	echo "Failed to compile"
	exit $?
elif [[ ! -f src/vegeta ]]; then
	echo "Failed to find expected compile output"
	exit 1
fi

docker build --no-cache -t vegeta:$VERSION .

#Cleanup after compilation
make -C src clean-vegeta
make -C src clean-vendor
