function install_mesh() {
  echo "Running linkerd check"
  linkerd check --pre
  check_result=$?
  if [[ $check_result -eq 0 ]]; then
       linkerd install | kubectl apply -f -
  else echo "Pre install check did not pass aborting"
  fi
	##$(linkerd install | kubectl apply -f -)
}

function uninstall_mesh() {
  linkerd uninstall | kubectl delete -f -
  kubectl delete namespace linkerd
}
