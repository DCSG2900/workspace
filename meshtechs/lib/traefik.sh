function install_mesh() {
  helm install traefik-mesh --create-namespace --namespace traefik-mesh traefik-mesh/traefik-mesh --set proxy.resources.limit.cpu='4000m' --set proxy.resources.limit.mem='4Gi'
}

function uninstall_mesh() {
  helm uninstall traefik-mesh --namespace traefik-mesh
  kubectl delete namespace traefik-mesh
  kubectl delete customresourcedefinitions.apiextensions.k8s.io trafficsplits.split.smi-spec.io
}
