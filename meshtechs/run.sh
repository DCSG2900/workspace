#!/bin/bash

CURRENT_MESH=$(<current.mesh)
function select_mesh() {
  if [[ $CURRENT_MESH == $1 ]]; then return
  fi
  if [[ $CURRENT_MESH != 'none' ]]; then
	  source "lib/$CURRENT_MESH.sh"
	  uninstall_mesh
  fi

  if [[ $1 != 'none' ]]; then
  	source "lib/$1.sh"
  	install_mesh
  fi
  CURRENT_MESH="$1"
  echo $1 > current.mesh
}

select_mesh 'linkerd2'

